package com.mercuriy94.themoviedb.presentation.common.di.activitybindings;


import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;

import dagger.MapKey;

/**
 * Created by nikita on 07.07.17.
 */
@MapKey
public @interface ActivityKey {

    Class<? extends BaseActivity> value();

}
