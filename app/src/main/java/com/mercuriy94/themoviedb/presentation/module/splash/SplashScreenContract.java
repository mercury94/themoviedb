package com.mercuriy94.themoviedb.presentation.module.splash;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView;

/**
 * Created by nikit on 11.11.2017.
 */

public class SplashScreenContract {

    public SplashScreenContract() {
        throw new RuntimeException("no instance please!");
    }

    public interface ISplashView extends IBaseView {

        @StateStrategyType(SkipStrategy.class)
        void navigateToFilmListScreen();

        @StateStrategyType(SkipStrategy.class)
        void closeScreen();

    }

    public static abstract class AbstractSplashPresenter extends BasePresenter<ISplashView> {

        public AbstractSplashPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }
    }


}
