package com.mercuriy94.themoviedb.presentation.common.di.activitybindings;

import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;
import com.mercuriy94.themoviedb.presentation.common.view.assembly.IBaseActivitySubcomponent;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by nikita on 07.07.17.
 */

@Module(subcomponents = {IBaseActivitySubcomponent.class})
public abstract class ActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(BaseActivity.class)
    public abstract ActivityComponentBuilder bindSignUpActivityComponent(IBaseActivitySubcomponent.Builder impl);

}
