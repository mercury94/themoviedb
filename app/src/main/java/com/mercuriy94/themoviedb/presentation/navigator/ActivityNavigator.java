package com.mercuriy94.themoviedb.presentation.navigator;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Pair;
import android.view.View;

import com.mercuriy94.themoviedb.presentation.model.MovieModel;
import com.mercuriy94.themoviedb.presentation.module.image.view.ImageViewActivity;
import com.mercuriy94.themoviedb.presentation.module.movie.view.MovieViewActivity;
import com.mercuriy94.themoviedb.presentation.module.movies.view.MoviesViewActivity;
import com.mercuriy94.themoviedb.presentation.module.overview.view.OverviewBiewActivity;

import javax.inject.Inject;

/**
 * Created by nikit on 11.11.2017.
 */

public class ActivityNavigator {

    @Inject
    public ActivityNavigator() {
        //empty
    }

    public void navigateToMoviesScreen(Context context) {
        context.startActivity(MoviesViewActivity.newIntent(context));
    }

    public void navigateToMovieScreen(Context context, MovieModel movie) {
        context.startActivity(MovieViewActivity.newIntent(context, movie));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void navigateToImageScreen(Activity activity,
                                      String posterPath,
                                      String movieTitle,
                                      Pair<View, String>[] animateViews) {
        activity.startActivity(ImageViewActivity.newIntent(activity, posterPath, movieTitle),
                ActivityOptions.makeSceneTransitionAnimation(activity, animateViews).toBundle());
    }


    public void navigateToImageScreen(Context context, String posterPath, String movieTitle) {
        context.startActivity(ImageViewActivity.newIntent(context, posterPath, movieTitle));
    }


    public void navigateToSettingsScreen(Activity activity) {
        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
    }


    public void navigateToOverviewScreen(Context context, String movieTitle, String overviewText) {
        context.startActivity(OverviewBiewActivity.newIntent(context, movieTitle, overviewText));
    }

}
