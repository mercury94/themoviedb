package com.mercuriy94.themoviedb.presentation.common.di.database;

import android.content.Context;
import android.support.annotation.NonNull;

import com.mercuriy94.themoviedb.data.entity.MyObjectBox;
import com.mercuriy94.themoviedb.presentation.module.app.assembly.AppModule;

import dagger.Module;
import dagger.Provides;
import io.objectbox.BoxStore;

/**
 * Created by nikit on 13.11.2017.
 */

@Module(includes = AppModule.class)
public class DatabaseModule {


    private final static String MY_OBJECT_BOX = "myobjectbox";


    @NonNull
    @Provides
    public BoxStore provideObjectBox(Context context) {
        return MyObjectBox.builder()
                .androidContext(context)
                .name(MY_OBJECT_BOX)
                .build();
    }


}
