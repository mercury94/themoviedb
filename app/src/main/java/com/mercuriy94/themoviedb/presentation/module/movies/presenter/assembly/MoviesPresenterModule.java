package com.mercuriy94.themoviedb.presentation.module.movies.presenter.assembly;

import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.PresenterModule;
import com.mercuriy94.themoviedb.presentation.module.movies.presenter.MoviesPresenter;

import dagger.Module;

/**
 * Created by nikit on 11.11.2017.
 */

@Module
public class MoviesPresenterModule extends PresenterModule<MoviesPresenter> {

    public MoviesPresenterModule(MoviesPresenter movieListPresenter) {
        super(movieListPresenter);
    }
}
