package com.mercuriy94.themoviedb.presentation.module.movies.view;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;
import com.mercuriy94.themoviedb.presentation.module.app.App;
import com.mercuriy94.themoviedb.presentation.module.movies.MoviesScreenContract;
import com.mercuriy94.themoviedb.presentation.module.movies.presenter.MoviesPresenter;
import com.mercuriy94.themoviedb.presentation.utils.NetworkHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mercuriy94.themoviedb.presentation.module.movies.MoviesScreenContract.SORTING_BY_POPULAR;
import static com.mercuriy94.themoviedb.presentation.module.movies.MoviesScreenContract.SORTING_BY_TOP_RATED;

/**
 * Created by nikit on 11.11.2017.
 */
public class MoviesViewActivity extends BaseActivity
        implements MoviesScreenContract.IMoviesView,
        PopupMenu.OnMenuItemClickListener,
        MoviesRecyclerAdapter.IMoviesRecyclerAdapterClickListener {

    @InjectPresenter
    MoviesScreenContract.AbstractMoviesPresenter mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.act_movies_recycler_view)
    RecyclerView mMoviesRecyclerView;
    @BindView(R.id.act_movies_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.act_movies_empty_list_view)
    LinearLayout mEmptyListContentLayout;
    @BindView(R.id.load_content_error_load_data)
    View mViewErrorLoadData;
    @BindView(R.id.load_content_pb)
    ProgressBar mProgressBar;
    @BindView(R.id.act_movies_content_layout)
    FrameLayout mRootContentLayout;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.act_movies_root_coordinator_layout)
    CoordinatorLayout mRootLayout;
    Snackbar mSnackbarDialog;

    private int mSpanCount = 0;
    private MoviesRecyclerAdapter mMoviesRecyclerAdapter;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;


    public static Intent newIntent(Context context) {
        return new Intent(context, MoviesViewActivity.class);
    }

    @ProvidePresenter
    MoviesScreenContract.AbstractMoviesPresenter providePresenter() {
        return new MoviesPresenter(App.getHasPresenterSubcomponentBuilders(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            setupWindowAnimations();
        }
        setContentView(R.layout.act_movies_layout);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        initViews();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        Transition open = TransitionInflater.from(this).inflateTransition(R.transition.open_movies_transition);
        Transition close = TransitionInflater.from(this).inflateTransition(R.transition.close_movies_transition);
        getWindow().setExitTransition(close);
        getWindow().setEnterTransition(open);
    }

    private IntentFilter getNetworkChangeStateIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        return intentFilter;
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mNetworkReceiver, getNetworkChangeStateIntentFilter());
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mNetworkReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPresenter.onClickBtnBack();
    }

    @Override
    public boolean onSupportNavigateUp() {
        mPresenter.onClickBtnBack();
        return true;
    }


    @Override
    public void close() {
        finish();
        System.exit(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.movies_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                mPresenter.onClickMenuSort();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showTitle(@StringRes int titleRes) {
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(titleRes);
    }

    @Override
    public void showSortPopupMenu(int currentSort) {
        View menuItemView = findViewById(R.id.action_sort);
        PopupMenu popupMenu = new PopupMenu(this, menuItemView);
        popupMenu.inflate(R.menu.movies_sort_menu);
        MenuItem menuItem = popupMenu.getMenu().getItem(currentSort);
        SpannableString s = new SpannableString(menuItem.getTitle());
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        s.setSpan(boldSpan, 0, menuItem.getTitle().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        menuItem.setTitle(s);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @OnClick({R.id.load_content_btn_retry_load_data, R.id.act_movies_empty_view_btn_retry})
    public void onClickBtnRetryLoadData() {
        mPresenter.onClickBtnRetryLoadData(false);
    }

    @Override
    public void showErrorLoadDataContent(boolean isFooter) {
        if (isFooter) mMoviesRecyclerAdapter.showErrorLadContentFooter();
        else mViewErrorLoadData.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorLoadDataContent() {
        mViewErrorLoadData.setVisibility(View.GONE);
        mMoviesRecyclerAdapter.hideErrorLoadContentFooter();
    }

    @Override
    public void clearData() {
        mMoviesRecyclerAdapter.updateList(new ArrayList<>());
        mMoviesRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptyListView() {
        mEmptyListContentLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyListView() {
        mEmptyListContentLayout.setVisibility(View.GONE);
    }

    private void initViews() {
        disableScrollBehavior();
        initRefreshLayout();
        initList();
    }

    private void enableScrollBehavior() {
        ViewGroup.LayoutParams layoutParams = mCollapsingToolbarLayout.getLayoutParams();
        if (layoutParams instanceof AppBarLayout.LayoutParams) {
            ((AppBarLayout.LayoutParams) layoutParams)
                    .setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL |
                            AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP |
                            AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
            mCollapsingToolbarLayout.setLayoutParams(layoutParams);
        }
    }

    private void disableScrollBehavior() {
        ViewGroup.LayoutParams layoutParams = mCollapsingToolbarLayout.getLayoutParams();
        if (layoutParams instanceof AppBarLayout.LayoutParams) {
            ((AppBarLayout.LayoutParams) layoutParams).setScrollFlags(0);
            mCollapsingToolbarLayout.setLayoutParams(layoutParams);
        }
    }

    private void initRefreshLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> mPresenter.onRefreshMovies());
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }

    @Override
    public void prepareScreen() {
        super.prepareScreen();
        mEmptyListContentLayout.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        mMoviesRecyclerAdapter.hidePendingFooter();
    }

    private void initList() {
        mMoviesRecyclerAdapter = new MoviesRecyclerAdapter();
        mMoviesRecyclerView.setAdapter(mMoviesRecyclerAdapter);
        mSpanCount = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? 2 : 3;
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(mSpanCount,
                StaggeredGridLayoutManager.VERTICAL);
        mMoviesRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);
        mMoviesRecyclerAdapter.setClickListener(this);
        mMoviesRecyclerView.addOnScrollListener(new MoviesRecyclerScrollListener());
    }

    @Override
    public void showFooterPendingLoadMovies() {
        mMoviesRecyclerAdapter.showPendingFooter();
    }

    @Override
    public void showPendingLoadMovies(boolean loadFromRefreshing) {
        disableScrollBehavior();
        if (loadFromRefreshing) {
            mSwipeRefreshLayout.setRefreshing(true);
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void hidePending() {
        mProgressBar.setVisibility(View.GONE);
        mMoviesRecyclerAdapter.hidePendingFooter();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showData(List<MovieModel> movies, DiffUtil.DiffResult diffResult) {
        mMoviesRecyclerAdapter.updateList(movies, diffResult);
        enableScrollBehavior();
    }

    private class MoviesRecyclerScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            int[] lastVisibleMoviesPositions = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager())
                    .findLastVisibleItemPositions(new int[mSpanCount]);
            mPresenter.onMoviesScrolled(lastVisibleMoviesPositions[mSpanCount - 1]);
        }
    }

    //region PopupMenu.OnMenuItemClickListener

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.movies_sort_by_popular) {
            mPresenter.onSelectedSortMenu(SORTING_BY_POPULAR);
        } else if (item.getItemId() == R.id.movies_sort_by_top_rated) {
            mPresenter.onSelectedSortMenu(SORTING_BY_TOP_RATED);
        } else {
            return false;
        }
        return true;
    }

    //endregion PopupMenu.OnMenuItemClickListener

    //region MoviesRecyclerAdapter.IMoviesRecyclerAdapterClickListener

    @Override
    public void onClickItem(int position, MovieModel movie) {
        mNavigator.navigateToMovieScreen(this, movie);
    }

    @Override
    public void onClickRetryLoadData() {
        mPresenter.onClickBtnRetryLoadData(true);
    }

    //endregion MoviesRecyclerAdapter.IMoviesRecyclerAdapterClickListener


    @Override
    public void showDialogRequestRefreshData() {
        releaseSnackbar();
        mSnackbarDialog = Snackbar
                .make(mRootLayout, R.string.network_available, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.refresh, view -> mPresenter.onClickBtnRefreshData())
                .addCallback(new Snackbar.Callback() {

                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        if (event == Snackbar.Callback.DISMISS_EVENT_SWIPE) {
                            mPresenter.onClickBtnNoRefreshData();
                        }
                    }
                });
        mSnackbarDialog.show();
    }

    @Override
    public void hideDialogRequestRefreshData() {
        releaseSnackbar();
    }

    @Override
    public void showDialogOpenSettingsConnection() {
        releaseSnackbar();
        mSnackbarDialog = Snackbar
                .make(mRootLayout, R.string.network_not_available, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.settings, view -> mPresenter.onClickBtnOpenSettings())
                .addCallback(new Snackbar.Callback() {

                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        if (event == Snackbar.Callback.DISMISS_EVENT_SWIPE) {
                            mPresenter.onClickBtnNoOpenSettings();
                        }
                    }
                });
        mSnackbarDialog.show();
    }

    @Override
    public void hideDialogOpenSettingsConnection() {
        releaseSnackbar();
    }

    private void releaseSnackbar() {
        if (mSnackbarDialog != null && mSnackbarDialog.isShown()) {
            mSnackbarDialog.dismiss();
            mSnackbarDialog = null;
        }
    }

    @Override
    public void navigateToSettingsScreen() {
        mNavigator.navigateToSettingsScreen(this);
    }

    private BroadcastReceiver mNetworkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isConnected = NetworkHelper.getConnectivityStatus(context) !=
                    NetworkHelper.TYPE_NOT_CONNECTED;

            mPresenter.onChangeNetworkState(isConnected);
        }
    };

}
