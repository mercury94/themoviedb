package com.mercuriy94.themoviedb.presentation.module.movie;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;

/**
 * Created by nikit on 11.11.2017.
 */

public class MovieScreenContract {

    private MovieScreenContract() {
        throw new RuntimeException("no instance please!");
    }


    public interface IMovieView extends IBaseView {

        @StateStrategyType(AddToEndSingleStrategy.class)
        void showMovieDetails(MovieModel movieModel);

        @StateStrategyType(SkipStrategy.class)
        void navigateToPosterView(String posterPath, String movieTitle);

        @StateStrategyType(AddToEndSingleStrategy.class)
        void showBackNavigationButton();

        @StateStrategyType(AddToEndSingleStrategy.class)
        void showTitle(String title);

        @StateStrategyType(SkipStrategy.class)
        void shareMovie(String title, String overview, String posterPathUrl);

        @StateStrategyType(SkipStrategy.class)
        void navigateToOverviewScreen(String title, String textOverview);

        @StateStrategyType(SkipStrategy.class)
        void close();
    }

    public static abstract class AbstractMoviePresenter extends BasePresenter<IMovieView> {

        public AbstractMoviePresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }

        public abstract void onClickOnPoster();

        public abstract void onClickBtnShare();

        public abstract void onErrorShare();

        public abstract void onChangeNetworkState(boolean isConnected);

        public abstract void onClickReadMoreOverview();

        public abstract void onClickBtnBack();

    }


}
