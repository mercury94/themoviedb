package com.mercuriy94.themoviedb.presentation.common.di.presenterbindings;


import com.mercuriy94.themoviedb.presentation.module.movies.presenter.MoviesPresenter;
import com.mercuriy94.themoviedb.presentation.module.movies.presenter.assembly.IMoviesPresenterSubcomponent;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


/**
 * Created by Nikita on 05.05.2017.
 */

@Module(subcomponents = {
        IMoviesPresenterSubcomponent.class
})
public abstract class PresenterBindingModule {

    @Binds
    @IntoMap
    @PresenterKey(MoviesPresenter.class)
    public abstract PresenterComponentBuilder bindMoviesPresenterComponent(IMoviesPresenterSubcomponent.Builder impl);


}
