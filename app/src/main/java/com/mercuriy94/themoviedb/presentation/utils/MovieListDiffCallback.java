package com.mercuriy94.themoviedb.presentation.utils;

import android.support.v7.util.DiffUtil;

import com.mercuriy94.themoviedb.presentation.model.MovieModel;

import java.util.List;

/**
 * Created by nikit on 12.11.2017.
 */

public class MovieListDiffCallback extends DiffUtil.Callback {

    private final List<MovieModel> mOldMovieList;
    private final List<MovieModel> mNewMovieList;
    private final int mCountFooters;

    public MovieListDiffCallback(List<MovieModel> oldMovieList, List<MovieModel> newMovieList, int countFooters) {
        mOldMovieList = oldMovieList;
        mNewMovieList = newMovieList;
        mCountFooters = countFooters;
    }

    @Override
    public int getOldListSize() {
        return mOldMovieList.size() + mCountFooters;
    }

    @Override
    public int getNewListSize() {
        return mNewMovieList.size() + mCountFooters;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        if (isFooter(oldItemPosition, newItemPosition)) return false;
        return mOldMovieList.get(oldItemPosition).getId() == mNewMovieList.get(newItemPosition).getId();
    }

    private boolean isFooter(int oldItemPosition, int newItemPosition) {
        return (oldItemPosition >= mOldMovieList.size()) ||
                (newItemPosition >= mNewMovieList.size());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldMovieList.get(oldItemPosition).equals(mNewMovieList.get(newItemPosition));
    }
}
