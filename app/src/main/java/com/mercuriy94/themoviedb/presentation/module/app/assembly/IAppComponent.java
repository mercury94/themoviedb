package com.mercuriy94.themoviedb.presentation.module.app.assembly;

import android.content.Context;

import com.mercuriy94.themoviedb.presentation.common.di.activitybindings.ActivityBindingModule;
import com.mercuriy94.themoviedb.presentation.common.di.database.DatabaseModule;
import com.mercuriy94.themoviedb.presentation.common.di.network.NetworkModule;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.PresenterBindingModule;
import com.mercuriy94.themoviedb.presentation.common.di.repository.RepositoryModule;
import com.mercuriy94.themoviedb.presentation.common.di.rxschedulers.RxSchedulerModule;
import com.mercuriy94.themoviedb.presentation.module.app.App;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {
        AppModule.class,
        PresenterBindingModule.class,
        ActivityBindingModule.class,
        DatabaseModule.class,
        NetworkModule.class,
        RxSchedulerModule.class,
        RepositoryModule.class
})

public interface IAppComponent {

    Context context();

    App inject(App application);

}
