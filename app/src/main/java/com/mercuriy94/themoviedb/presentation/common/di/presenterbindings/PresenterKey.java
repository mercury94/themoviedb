package com.mercuriy94.themoviedb.presentation.common.di.presenterbindings;


import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;

import dagger.MapKey;

/**
 * Created by Nikita on 05.05.2017.
 */

@MapKey
public @interface PresenterKey {

    Class<? extends BasePresenter> value();

}
