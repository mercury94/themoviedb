package com.mercuriy94.themoviedb.presentation.common.di.presenterbindings;


import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;

/**
 * Created by Nikita on 05.05.2017.
 */

public interface HasPresenterSubcomponentBuilders {

    PresenterComponentBuilder getPresenterComponentBuilder(Class<? extends BasePresenter> presenterClass);

}
