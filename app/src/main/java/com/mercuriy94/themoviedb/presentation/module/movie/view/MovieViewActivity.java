package com.mercuriy94.themoviedb.presentation.module.movie.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;
import com.mercuriy94.themoviedb.presentation.common.view.Layout;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;
import com.mercuriy94.themoviedb.presentation.module.app.App;
import com.mercuriy94.themoviedb.presentation.module.movie.MovieScreenContract;
import com.mercuriy94.themoviedb.presentation.module.movie.presenter.MoviePresenter;
import com.mercuriy94.themoviedb.presentation.utils.GlideApp;
import com.mercuriy94.themoviedb.presentation.utils.NetworkHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by nikit on 11.11.2017.
 */
@Layout(R.layout.act_movie_layout)
public class MovieViewActivity extends BaseActivity implements MovieScreenContract.IMovieView {

    public static final String EXTRA_MOVIE = "extra_movie";

    @InjectPresenter
    MovieScreenContract.AbstractMoviePresenter mPresenter;

    @BindView(R.id.act_movie_iv_book)
    ImageView mIvPoster;
    @BindView(R.id.act_movie_root_layout)
    ConstraintLayout mRootLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.act_movie_tv_overview)
    TextView mTvOverview;
    @BindView(R.id.act_movie_tv_title)
    TextView mTvTitle;
    @BindView(R.id.act_movie_tv_release_date)
    TextView mTvReleaseDate;
    @BindView(R.id.act_movie_tv_title_vote_average)
    TextView mTvVoteRageTitle;
    @BindView(R.id.act_movie_rb_vote_average)
    RatingBar mRbVoteAverage;

    public static Intent newIntent(Context context, MovieModel movie) {
        Intent intent = new Intent(context, MovieViewActivity.class);
        intent.putExtra(EXTRA_MOVIE, movie);
        return intent;
    }

    @ProvidePresenter
    MovieScreenContract.AbstractMoviePresenter providePresenter() {
        return new MoviePresenter(App.getHasPresenterSubcomponentBuilders(this),
                getIntent().getParcelableExtra(EXTRA_MOVIE));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(mToolbar);
    }

    private IntentFilter getNetworkChangeStateIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        return intentFilter;
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mNetworkReceiver, getNetworkChangeStateIntentFilter());
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mNetworkReceiver);
    }


    @OnClick(R.id.act_movie_iv_book)
    public void onClickOnPoster() {
        mPresenter.onClickOnPoster();
    }

    @OnClick(R.id.act_movie_btn_share)
    public void onClickBtnShare() {
        mPresenter.onClickBtnShare();
    }

    @OnClick(R.id.act_movie_btn_read_more_overview)
    public void onClickBtnReaMoreOverview() {
        mPresenter.onClickReadMoreOverview();
    }

    public void showPoster(String imagePath) {
        GlideApp.with(this)
                .load(imagePath)
                .placeholder(R.drawable.image_paceholder)
                .into(mIvPoster);
    }


    @Override
    public void navigateToOverviewScreen(String title, String textOverview) {
        mNavigator.navigateToOverviewScreen(this, title, textOverview);
    }

    //region share

    @Override
    public void shareMovie(String title, String overview, String posterPathUrl) {

        Uri bmpUri = getLocalBitmapUri(mIvPoster);
        if (bmpUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.putExtra(Intent.EXTRA_TEXT, overview);
            shareIntent.setType("image/*");
            startActivity(Intent.createChooser(shareIntent, title));
        } else {
            mPresenter.onErrorShare();
        }
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (!(drawable instanceof BitmapDrawable)) return null;
        bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        return getUriFromBitmap(bmp);
    }

    private Uri getUriFromBitmap(Bitmap bitmap) {
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.close();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                bmpUri = FileProvider.getUriForFile(MovieViewActivity.this,
                        "com.mercuriy94.themoviedb.fileprovider", file);
            } else {
                bmpUri = Uri.fromFile(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    //endregion share

    @Override
    public void showMovieDetails(MovieModel movieModel) {
        showPoster(movieModel.getPosterFullPath());
        mTvTitle.setText(movieModel.getTitle());
        mTvOverview.setText(movieModel.getOverview());
        if (movieModel.getReleaseDate() != null) {
            mTvReleaseDate.setText(getString(R.string.release_date_text, movieModel.getReleaseDate()));
        } else {
            mTvReleaseDate.setText("");
        }
        mTvVoteRageTitle.setText(getString(R.string.act_movie_vote_average, movieModel.getVoteAverage()));
        mRbVoteAverage.setRating(movieModel.getVoteAverage());
    }

    @Override
    public void showTitle(String title) {
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(title);
    }

    @Override
    public void showBackNavigationButton() {
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void navigateToPosterView(String posterPath, String movieTitle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            List<Pair<View, String>> pairs = new ArrayList<>();
            for (int i = 0; i < mToolbar.getChildCount(); i++) {
                View viewToolbar = mToolbar.getChildAt(i);
                if (viewToolbar instanceof TextView) {
                    viewToolbar.setTransitionName("title_transition");
                    pairs.add(Pair.create(viewToolbar, viewToolbar.getTransitionName()));
                    continue;
                }

                if (viewToolbar instanceof ImageButton) {
                    viewToolbar.setTransitionName("back_btn_transition");
                    pairs.add(Pair.create(viewToolbar, viewToolbar.getTransitionName()));
                }
            }

            pairs.add(Pair.create(mIvPoster, mIvPoster.getTransitionName()));
            //noinspection unchecked
            mNavigator.navigateToImageScreen(this,
                    posterPath,
                    movieTitle,
                    pairs.toArray(new Pair[pairs.size()]));
        } else {
            mNavigator.navigateToImageScreen(this, posterPath, movieTitle);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPresenter.onClickBtnBack();
    }

    @Override
    public boolean onSupportNavigateUp() {
        mPresenter.onClickBtnBack();
        return true;
    }


    @Override
    public void close() {
        finish();
    }

    private BroadcastReceiver mNetworkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isConnected = NetworkHelper.getConnectivityStatus(context) !=
                    NetworkHelper.TYPE_NOT_CONNECTED;

            mPresenter.onChangeNetworkState(isConnected);
        }
    };

}
