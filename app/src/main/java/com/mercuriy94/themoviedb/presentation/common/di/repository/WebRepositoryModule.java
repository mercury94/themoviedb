package com.mercuriy94.themoviedb.presentation.common.di.repository;

import android.support.annotation.NonNull;

import com.mercuriy94.themoviedb.data.repositry.movie.web.IMovieWebRepository;
import com.mercuriy94.themoviedb.data.repositry.movie.web.IMovieWebService;
import com.mercuriy94.themoviedb.data.repositry.movie.web.MovieWebRepository;
import com.mercuriy94.themoviedb.presentation.common.di.network.NetworkModule;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by nikit on 11.11.2017.
 */

@Module(includes = {NetworkModule.class, WebRepositoryModule.Declarations.class})
public class WebRepositoryModule {

    @NonNull
    @Provides
    protected IMovieWebService provideMovieWebService(Retrofit retrofit) {
        return retrofit.create(IMovieWebService.class);
    }

    @Module
    public static abstract class Declarations {

        @NonNull
        @Binds
        protected abstract IMovieWebRepository bindMovieWebRepository(MovieWebRepository movieWebRepositoryImpl);

    }
}

