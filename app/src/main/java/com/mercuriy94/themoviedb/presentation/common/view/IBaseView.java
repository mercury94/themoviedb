package com.mercuriy94.themoviedb.presentation.common.view;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by nikit on 11.11.2017.
 */

public interface IBaseView extends MvpView {

    @StateStrategyType(value = AddToEndSingleByTagStrategy.class, tag = AddToEndSingleByTagStrategy.TAG_SHOW)
    void showPending(String message);

    @StateStrategyType(value = AddToEndSingleByTagStrategy.class, tag = AddToEndSingleByTagStrategy.TAG_SHOW)
    void showPending(@StringRes int id);

    @StateStrategyType(value = AddToEndSingleByTagStrategy.class, tag = AddToEndSingleByTagStrategy.TAG_HIDE)
    void hidePending();

    @StateStrategyType(value = SkipStrategy.class)
    void prepareScreen();

    @StateStrategyType(value = SkipStrategy.class)
    void showLongToast(String message);

    @StateStrategyType(value = SkipStrategy.class)
    void showShortToast(String message);

    @StateStrategyType(value = SkipStrategy.class)
    void showLongToast(@StringRes int stringRes);

    @StateStrategyType(value = SkipStrategy.class)
    void showShortToast(@StringRes int stringRes);

}
