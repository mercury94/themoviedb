package com.mercuriy94.themoviedb.presentation.module.movies.view;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;
import com.mercuriy94.themoviedb.presentation.utils.GlideApp;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nikit on 11.11.2017.
 */

public class MoviesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "MoviesRecyclerAdapter";

    private final int TYPE_ITEM = 0;
    private final int TYPE_FOOTER = 1;
    private final int mCountFooters = 1;
    private boolean mVisiblePendingFooter = false;
    private boolean mVisibleErrorLoadDataFooter = false;
    private List<MovieModel> mMovies;

    private IMoviesRecyclerAdapterClickListener mClickListener;

    public MoviesRecyclerAdapter() {
        mMovies = new ArrayList<>();
    }

    public interface IMoviesRecyclerAdapterClickListener {

        void onClickItem(int position, MovieModel movie);

        void onClickRetryLoadData();
    }

    public IMoviesRecyclerAdapterClickListener getClickListener() {
        return mClickListener;
    }

    public void setClickListener(IMoviesRecyclerAdapterClickListener clickListener) {
        mClickListener = clickListener;
    }

    public void updateList(@NonNull List<MovieModel> movies) {
        mMovies = movies;
    }

    public void updateList(@NonNull List<MovieModel> movies, DiffUtil.DiffResult diffResult) {
        int oldSize = mMovies == null ? 0 : mMovies.size();
        mMovies = movies;
        if (oldSize == 0) notifyDataSetChanged();
        else diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemViewType(int position) {
        if (!isFooter(position)) return TYPE_ITEM;
        else return TYPE_FOOTER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            return new MovieViewHolderItem(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.act_movies_recycler_li_layout, parent, false));
        } else {
            return new MovieViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.load_data_content_layout, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isFooter(position)) ((MovieViewHolderFooter) holder).bind();
        else ((MovieViewHolderItem) holder).bind(mMovies.get(position));
    }

    public boolean isVisiblePendingFooter() {
        return mVisiblePendingFooter;
    }


    public boolean isVisibleErrorLoadDataFooter() {
        return mVisibleErrorLoadDataFooter;
    }


    public void showPendingFooter() {
        mVisiblePendingFooter = true;
        mVisibleErrorLoadDataFooter = false;
        notifyItemChanged(getItemCount() - 1);
    }

    public void hidePendingFooter() {
        mVisiblePendingFooter = false;
        notifyItemChanged(getItemCount() - 1);
    }


    public void showErrorLadContentFooter() {
        mVisibleErrorLoadDataFooter = true;
        mVisiblePendingFooter = false;
        notifyItemChanged(getItemCount() - 1);
    }

    public void hideErrorLoadContentFooter() {
        mVisibleErrorLoadDataFooter = false;
        notifyItemChanged(getItemCount() - 1);
    }


    @Override
    public int getItemCount() {
        return mMovies.size() + mCountFooters;
    }

    private boolean isFooter(int position) {
        return (position > (mMovies.size() - 1));
    }

    class MovieViewHolderItem extends RecyclerView.ViewHolder {

        @BindView(R.id.act_movies_recycler_item_iv_preview)
        ImageView mIvPoster;
        @BindView(R.id.acr_movies_tv_title)
        TextView mTvTitle;

        public MovieViewHolderItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @OnClick(R.id.act_movies_li_root_layout)
        void onClickItem() {
            if (mClickListener != null) {
                mClickListener.onClickItem(getAdapterPosition(), mMovies.get(getAdapterPosition()));
            }
        }

        public void bind(MovieModel movie) {

            mTvTitle.setText(movie.getTitle());

            GlideApp.with(itemView.getContext())
                    .load(movie.getPreviewImageFullPath())
                    .placeholder(R.drawable.image_paceholder)
                    .into(mIvPoster);
        }
    }

    class MovieViewHolderFooter extends RecyclerView.ViewHolder {

        @BindView(R.id.load_content_error_load_data)
        View mViewErrorLoadData;
        @BindView(R.id.load_content_pb)
        ProgressBar mProgressBar;

        public MovieViewHolderFooter(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @OnClick(R.id.load_content_btn_retry_load_data)
        public void onClickBtnRetryLoadData() {
            if (mClickListener != null) mClickListener.onClickRetryLoadData();
        }


        private void bind() {

            boolean visible = false;

            if (isVisiblePendingFooter()) {
                visible = true;
                mProgressBar.setVisibility(View.VISIBLE);
                mViewErrorLoadData.setVisibility(View.GONE);
            } else if (isVisibleErrorLoadDataFooter()) {
                visible = true;
                mProgressBar.setVisibility(View.GONE);
                mViewErrorLoadData.setVisibility(View.VISIBLE);
            }


            StaggeredGridLayoutManager.LayoutParams layoutParams =
                    new StaggeredGridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            mMovies == null || mMovies.size() == 0 ? 0 : visible ?
                                    ViewGroup.LayoutParams.WRAP_CONTENT : 0);
            layoutParams.setFullSpan(true);
            itemView.setLayoutParams(layoutParams);
        }


    }


}
