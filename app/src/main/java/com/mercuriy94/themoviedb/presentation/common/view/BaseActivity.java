package com.mercuriy94.themoviedb.presentation.common.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.mercuriy94.themoviedb.presentation.common.di.activitybindings.HasActivitySubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.view.assembly.BaseActivityModule;
import com.mercuriy94.themoviedb.presentation.common.view.assembly.IBaseActivitySubcomponent;
import com.mercuriy94.themoviedb.presentation.module.app.App;
import com.mercuriy94.themoviedb.presentation.navigator.ActivityNavigator;

import java.lang.annotation.Annotation;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by nikit on 11.11.2017.
 */

public abstract class BaseActivity extends MvpAppCompatActivity implements IBaseView {

    protected ProgressDialog mProgressDialog;

    @Inject
    protected ActivityNavigator mNavigator;

    //region MvpAppCompatActivity

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindLayout();
        inject(App.getHasActivitySubcomponentBuilders(this));
    }

    //endregion MvpAppCompatActivity

    //region IBaseView

    @Override
    public void prepareScreen() {
        //do override
    }

    @Override
    public void showPending(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void showPending(@StringRes int id) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(id));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void hidePending() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }


    @Override
    public void showLongToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showShortToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLongToast(@StringRes int stringRes) {
        Toast.makeText(this, stringRes, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showShortToast(@StringRes int stringRes) {
        Toast.makeText(this, stringRes, Toast.LENGTH_SHORT).show();
    }

    //endregion IBaseView

    //region this is class

    protected void bindLayout() {
        Class cls = getClass();
        if (!cls.isAnnotationPresent(Layout.class)) return;
        Annotation annotation = cls.getAnnotation(Layout.class);
        Layout layout = (Layout) annotation;
        setContentView(layout.value());
        ButterKnife.bind(this);
    }

    protected void inject(@NonNull HasActivitySubcomponentBuilders builders) {
        ((IBaseActivitySubcomponent.Builder) builders.getActivityComponentBuilder(BaseActivity.class))
                .activityModule(new BaseActivityModule(this))
                .build()
                .injectMembers(this);

    }

    protected String getTag() {
        return getClass().getName();
    }

    //endregion this is class

}
