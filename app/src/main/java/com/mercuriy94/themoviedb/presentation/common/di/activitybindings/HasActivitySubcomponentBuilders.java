package com.mercuriy94.themoviedb.presentation.common.di.activitybindings;


import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;

/**
 * Created by nikita on 07.07.17.
 */

public interface HasActivitySubcomponentBuilders {

    ActivityComponentBuilder getActivityComponentBuilder(Class<? extends BaseActivity> presenterClass);

}
