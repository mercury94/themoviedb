package com.mercuriy94.themoviedb.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nikit on 11.11.2017.
 */

public class MovieModel implements Parcelable {

    private long mId;
    private String mPreviewImageFullPath;
    private String mPosterPath;
    private String mPosterFullPath;
    private boolean mAdult;
    private String mOverview;
    private String mReleaseDate;
    private String mOriginalTitle;
    private String mOriginalLanguage;
    private String mTitle;
    private String mBackDropPath;
    private float mPopularity;
    private int mVoteCount;
    private boolean mVideo;
    private float mVoteAverage;

    public MovieModel() {
    }

    private MovieModel(Parcel in) {
        mId = in.readLong();
        mPreviewImageFullPath = in.readString();
        mPosterPath = in.readString();
        mPosterFullPath = in.readString();
        mAdult = in.readByte() != 0;
        mOverview = in.readString();
        mReleaseDate = in.readString();
        mOriginalTitle = in.readString();
        mOriginalLanguage = in.readString();
        mTitle = in.readString();
        mBackDropPath = in.readString();
        mPopularity = in.readFloat();
        mVoteCount = in.readInt();
        mVideo = in.readByte() != 0;
        mVoteAverage = in.readFloat();
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getPreviewImageFullPath() {
        return mPreviewImageFullPath;
    }

    public void setPreviewImageFullPath(String previewImageFullPath) {
        mPreviewImageFullPath = previewImageFullPath;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public String getPosterFullPath() {
        return mPosterFullPath;
    }

    public void setPosterFullPath(String posterFullPath) {
        mPosterFullPath = posterFullPath;
    }

    public boolean isAdult() {
        return mAdult;
    }

    public void setAdult(boolean adult) {
        mAdult = adult;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public String getOriginalTitle() {
        return mOriginalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        mOriginalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return mOriginalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        mOriginalLanguage = originalLanguage;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBackDropPath() {
        return mBackDropPath;
    }

    public void setBackDropPath(String backDropPath) {
        mBackDropPath = backDropPath;
    }

    public float getPopularity() {
        return mPopularity;
    }

    public void setPopularity(float popularity) {
        mPopularity = popularity;
    }

    public int getVoteCount() {
        return mVoteCount;
    }

    public void setVoteCount(int voteCount) {
        mVoteCount = voteCount;
    }

    public boolean isVideo() {
        return mVideo;
    }

    public void setVideo(boolean video) {
        mVideo = video;
    }

    public float getVoteAverage() {
        return mVoteAverage;
    }

    public void setVoteAverage(float voteAverage) {
        mVoteAverage = voteAverage;
    }

    public static final Creator<MovieModel> CREATOR = new Creator<MovieModel>() {
        @Override
        public MovieModel createFromParcel(Parcel in) {
            return new MovieModel(in);
        }

        @Override
        public MovieModel[] newArray(int size) {
            return new MovieModel[size];
        }
    };


    //region Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(mId);
        parcel.writeString(mPreviewImageFullPath);
        parcel.writeString(mPosterPath);
        parcel.writeString(mPosterFullPath);
        parcel.writeByte((byte) (isAdult() ? 1 : 0));
        parcel.writeString(mOverview);
        parcel.writeString(mReleaseDate);
        parcel.writeString(mOriginalTitle);
        parcel.writeString(mOriginalLanguage);
        parcel.writeString(mTitle);
        parcel.writeString(mBackDropPath);
        parcel.writeFloat(mPopularity);
        parcel.writeInt(mVoteCount);
        parcel.writeByte((byte) (isVideo() ? 1 : 0));
        parcel.writeFloat(mVoteAverage);
    }

    //endregion Parcelable
}
