package com.mercuriy94.themoviedb.presentation.common.di.activitybindings;


import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;

import dagger.MembersInjector;

/**
 * Created by nikita on 07.07.17.
 */

public interface ActivityComponent<Activity extends BaseActivity> extends MembersInjector<Activity> {
}
