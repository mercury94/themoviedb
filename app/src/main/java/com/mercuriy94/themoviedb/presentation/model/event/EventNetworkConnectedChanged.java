package com.mercuriy94.themoviedb.presentation.model.event;

/**
 * Created by nikit on 13.11.2017.
 */

public class EventNetworkConnectedChanged {

    private final boolean mNetworkConnected;

    public EventNetworkConnectedChanged(boolean networkConnected) {
        mNetworkConnected = networkConnected;
    }

    public boolean isNetworkConnected() {
        return mNetworkConnected;
    }
}
