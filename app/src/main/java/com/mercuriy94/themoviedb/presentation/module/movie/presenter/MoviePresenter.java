package com.mercuriy94.themoviedb.presentation.module.movie.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;
import com.mercuriy94.themoviedb.presentation.module.movie.MovieScreenContract;

/**
 * Created by nikit on 11.11.2017.
 */

@InjectViewState
public class MoviePresenter extends MovieScreenContract.AbstractMoviePresenter {

    MovieModel mMovieModel;

    public MoviePresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders, MovieModel movieModel) {
        super(presenterSubcomponentBuilders);
        mMovieModel = movieModel;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().showBackNavigationButton();
        getViewState().showTitle(mMovieModel.getTitle());
        getViewState().showMovieDetails(mMovieModel);
    }

    @Override
    public void onClickOnPoster() {
        getViewState().navigateToPosterView(mMovieModel.getPosterFullPath(), mMovieModel.getTitle());
    }

    @Override
    public void onClickBtnBack() {
        getViewState().close();
    }

    @Override
    public void onClickBtnShare() {
        getViewState().shareMovie(mMovieModel.getTitle(),
                String.format("%s\n%s", mMovieModel.getTitle(), mMovieModel.getOverview()),
                mMovieModel.getPosterFullPath());
    }

    @Override
    public void onErrorShare() {
        getViewState().showShortToast(R.string.default_error_message);
    }

    @Override
    public void onChangeNetworkState(boolean isConnected) {
        getViewState().showMovieDetails(mMovieModel);
    }

    @Override
    public void onClickReadMoreOverview() {
        getViewState().navigateToOverviewScreen(mMovieModel.getTitle(), mMovieModel.getOverview());
    }
}
