package com.mercuriy94.themoviedb.presentation.module.image.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.module.image.ImageViewContract;

/**
 * Created by nikit on 12.11.2017.
 */
@InjectViewState
public class ImagePresenter extends ImageViewContract.AbstractImagePresenter {

    private String mPosterPath;
    private String mMovieTitle;

    public ImagePresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders,
                          @NonNull String posterPath,
                          @NonNull String movieTitle) {
        super(presenterSubcomponentBuilders);
        mPosterPath = posterPath;
        mMovieTitle = movieTitle;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().showBackNavigationButton();
        getViewState().showTitle(mMovieTitle);
        getViewState().showPoster(mPosterPath);
    }

    @Override
    public void onClickBtnBack() {
        getViewState().close();
    }
}
