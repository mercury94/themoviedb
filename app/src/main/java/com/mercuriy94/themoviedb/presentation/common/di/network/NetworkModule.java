package com.mercuriy94.themoviedb.presentation.common.di.network;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.mercuriy94.themoviedb.BuildConfig;
import com.mercuriy94.themoviedb.data.constant.WebServiceConstant;
import com.mercuriy94.themoviedb.presentation.module.app.assembly.AppModule;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = AppModule.class)
public class NetworkModule {

    @NonNull
    @Provides
    protected Retrofit provideRetrofit(
            OkHttpClient okHttpClient,
            Retrofit.Builder baseRetrofitBuilder) {
        return baseRetrofitBuilder
                .client(okHttpClient)
                .build();
    }

    @NonNull
    @Provides
    public Retrofit.Builder provideBaseRetrofitBuilder(Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(WebServiceConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    @NonNull
    @Provides
    public OkHttpClient.Builder provideBaseOkHttpBuilder() {
        return new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Log.INFO)
                        .request("Request")
                        .response("Response")
                        .build());
    }

    @NonNull
    @Provides
    public OkHttpClient provideOkHttpClient(OkHttpClient.Builder baseBuilder) {
        return baseBuilder.build();
    }

    @NonNull
    @Provides
    public Gson provideGson() {
        return new Gson();
    }

}
