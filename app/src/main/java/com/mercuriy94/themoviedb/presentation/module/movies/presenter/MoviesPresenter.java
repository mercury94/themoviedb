package com.mercuriy94.themoviedb.presentation.module.movies.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.text.TextUtils;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.data.entity.Movie;
import com.mercuriy94.themoviedb.data.utils.INetworkHelper;
import com.mercuriy94.themoviedb.domain.common.DefaultObserver;
import com.mercuriy94.themoviedb.domain.movie.FetchMoviesInteractor;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.mapper.MovieMapper;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;
import com.mercuriy94.themoviedb.presentation.module.movies.MoviesScreenContract;
import com.mercuriy94.themoviedb.presentation.module.movies.presenter.assembly.IMoviesPresenterSubcomponent;
import com.mercuriy94.themoviedb.presentation.module.movies.presenter.assembly.MoviesPresenterModule;
import com.mercuriy94.themoviedb.presentation.utils.MovieListDiffCallback;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.mercuriy94.themoviedb.presentation.module.movies.MoviesScreenContract.SORTING_BY_POPULAR;

/**
 * Created by nikit on 11.11.2017.
 */
@InjectViewState
public class MoviesPresenter extends MoviesScreenContract.AbstractMoviesPresenter {

    public static final String TAG = "MoviesPresenter";


    //порог прокручивания, после преодоления которого начинается загрузка следюущей старницы
    private static final int SCROLL_THRESHOLD = 13;

    @Inject
    FetchMoviesInteractor mFetchPopularMoviesInteractor;
    @Inject
    MovieMapper mMovieMapper;
    @Inject
    INetworkHelper mNetworkHelper;

    @Nullable
    List<MovieModel> mMovieModels;

    private boolean mThresholdScrollListener;
    private int mCurrentSorting = SORTING_BY_POPULAR;
    private boolean mNetworkConnectedLastStatus;


    public MoviesPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        super(presenterSubcomponentBuilders);
    }

    @Override
    protected void inject(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        super.inject(presenterSubcomponentBuilders);
        ((IMoviesPresenterSubcomponent.Builder) presenterSubcomponentBuilders.getPresenterComponentBuilder(MoviesPresenter.class))
                .presenterModule(new MoviesPresenterModule(this))
                .build()
                .injectMembers(this);
    }

    public boolean isThresholdScrollListener() {
        return mThresholdScrollListener;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().showTitle(R.string.title_movies);
        fetchMovies(false, false);
    }

    @Override
    public void attachView(MoviesScreenContract.IMoviesView view) {
        super.attachView(view);
        mNetworkConnectedLastStatus = mNetworkHelper.isNetworkConnected();
        if (!isNetworkConnectedLastStatus()) getViewState().showDialogOpenSettingsConnection();
    }

    public boolean isNetworkConnectedLastStatus() {
        return mNetworkConnectedLastStatus;
    }

    private void fetchMovies(boolean loadFromUserScrolled, boolean loadFromRefreshing) {
        if (mCurrentSorting == SORTING_BY_POPULAR) {
            mFetchPopularMoviesInteractor.execute(new FetchMoviesDisposable(loadFromUserScrolled, loadFromRefreshing),
                    new FetchMoviesInteractor.Params(FetchMoviesInteractor.SORT_BY_POPULAR));
        } else {
            mFetchPopularMoviesInteractor.execute(new FetchMoviesDisposable(loadFromUserScrolled, loadFromRefreshing),
                    new FetchMoviesInteractor.Params(FetchMoviesInteractor.SORT_BY_TOP_RATED));
        }
    }

    @Override
    public void onClickMenuSort() {
        getViewState().showSortPopupMenu(mCurrentSorting);
    }

    @Override
    public void onMoviesScrolled(int lastVisibleMoviesPosition) {
        if (isThresholdScrollListener() && isBreakTreshold(lastVisibleMoviesPosition)) {
            fetchMovies(true, false);
        }
    }

    private boolean isBreakTreshold(int lastVisibleMoviesPosition) {
        return mMovieModels == null || ((mMovieModels.size() - lastVisibleMoviesPosition) <= SCROLL_THRESHOLD);
    }

    @Override
    public void onRefreshMovies() {
        releaseMovies();
        fetchMovies(false, true);
    }

    @Override
    public void onSelectedSortMenu(int sort) {
        if (mCurrentSorting != sort) {
            mCurrentSorting = sort;
            releaseMovies();
            fetchMovies(false, false);
        }
    }

    @Override
    public void onClickBtnRetryLoadData(boolean isFooter) {
        fetchMovies(isFooter, false);
    }


    /**
     * Подготовка данных для отображения, с расчетом обновления только необходимых пунктов
     */
    private void prepareMoviesForView(List<Movie> movies) {
        Observable.just(movies)
                .map(mMovieMapper::execute)
                .flatMap(movieModels -> {
                    if (mMovieModels == null) {
                        return calculateDiffLists(new ArrayList<>(), movieModels);
                    } else {
                        movieModels.addAll(0, mMovieModels);
                        return calculateDiffLists(mMovieModels, movieModels);
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handlePrepareMovies);
    }

    private void handlePrepareMovies(DiffListsResult diffListsResult) {
        if (diffListsResult.mMovieModels.size() > (mMovieModels != null ?
                mMovieModels.size() : 0)) {

            getViewState().showData(new ArrayList<>(diffListsResult.mMovieModels),
                    diffListsResult.mDiffResult);
            mThresholdScrollListener = true;
        } else {
            mThresholdScrollListener = false;
            if (mMovieModels == null || mMovieModels.isEmpty()) getViewState().showEmptyListView();

            if (!mNetworkHelper.isNetworkConnected()) {
                getViewState().showDialogOpenSettingsConnection();
            }
        }
        mMovieModels = diffListsResult.mMovieModels;
        getViewState().hidePending();
    }


    private Observable<DiffListsResult> calculateDiffLists(List<MovieModel> oldList, List<MovieModel> newList) {
        return Observable.zip(Observable.just(newList),
                Observable.fromCallable(() -> DiffUtil.calculateDiff(
                        new MovieListDiffCallback(oldList, newList, 1), false)),
                DiffListsResult::new);
    }

    //Поностью отпустить отслеживание списка и с очиткой данных на view
    private void releaseMovies() {
        mMovieModels = null;
        getViewState().clearData();
        mFetchPopularMoviesInteractor.releasePage();
    }


    private class DiffListsResult {

        private final List<MovieModel> mMovieModels;

        //Результат разницы нового и старого списка, тем обновляя только необходимые элементы списка
        private final DiffUtil.DiffResult mDiffResult;

        private DiffListsResult(List<MovieModel> movieModels, DiffUtil.DiffResult diffResult) {
            mMovieModels = movieModels;
            mDiffResult = diffResult;
        }

    }


    private class FetchMoviesDisposable extends DefaultObserver<List<Movie>> {

        private final boolean mLoadFromUserScrolled;
        private final boolean mLoadFromRefreshing;

        private FetchMoviesDisposable(boolean loadFromUserScrolled, boolean loadFromRefreshing) {
            mLoadFromUserScrolled = loadFromUserScrolled;
            mLoadFromRefreshing = loadFromRefreshing;
        }

        private boolean isLoadFromUserScrolled() {
            return mLoadFromUserScrolled;
        }

        private boolean isLoadFromRefreshing() {
            return mLoadFromRefreshing;
        }

        @Override
        protected void onStart() {
            mThresholdScrollListener = false;
            getViewState().hideEmptyListView();
            getViewState().hideErrorLoadDataContent();
            if (isLoadFromUserScrolled()) getViewState().showFooterPendingLoadMovies();
            else getViewState().showPendingLoadMovies(isLoadFromRefreshing());
        }

        @Override
        public void onNext(List<Movie> movies) {
            prepareMoviesForView(movies);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            getViewState().hidePending();
            getViewState().showErrorLoadDataContent(mMovieModels != null && !mMovieModels.isEmpty());
        }

        @Override
        protected void onHttpError(Throwable throwable, String message) {
            if (!TextUtils.isEmpty(message)) getViewState().showShortToast(message);
            else getViewState().showShortToast(R.string.default_error_message);
        }

        @Override
        protected void onUnknownError(Throwable throwable, String message) {
            if (!TextUtils.isEmpty(message)) getViewState().showShortToast(message);
            else getViewState().showShortToast(R.string.default_error_message);
        }
    }

    @Override
    public void onClickBtnOpenSettings() {
        getViewState().hideDialogOpenSettingsConnection();
        getViewState().navigateToSettingsScreen();
    }

    @Override
    public void onClickBtnNoOpenSettings() {
        getViewState().hideDialogOpenSettingsConnection();
    }

    @Override
    public void onClickBtnRefreshData() {
        releaseMovies();
        getViewState().hideDialogRequestRefreshData();
        fetchMovies(false, false);
    }

    @Override
    public void onClickBtnNoRefreshData() {
        getViewState().hideDialogRequestRefreshData();
    }

    @Override
    public void onChangeNetworkState(boolean isConnected) {
        if (mNetworkConnectedLastStatus != isConnected) {
            if (isConnected) {
                getViewState().showDialogRequestRefreshData();
                Log.d(TAG, "onNetworkConnected");
                mNetworkConnectedLastStatus = true;
            } else {
                getViewState().showDialogOpenSettingsConnection();
                Log.d(TAG, "onNetworkDisconnected");
                mNetworkConnectedLastStatus = false;
            }
        }
        mNetworkConnectedLastStatus = isConnected;
    }

    @Override
    public void onClickBtnBack() {
        getViewState().close();
    }
}

