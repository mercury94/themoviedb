package com.mercuriy94.themoviedb.presentation.common.di.activitybindings;

/**
 * Created by nikita on 07.07.17.
 */

public interface ActivityComponentBuilder<M extends ActivityModule, C extends ActivityComponent> {

    ActivityComponentBuilder<M, C> activityModule(M module);

    C build();
}
