package com.mercuriy94.themoviedb.presentation.module.splash.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.module.splash.SplashScreenContract;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by nikit on 11.11.2017.
 */

@InjectViewState
public class SplashPresenter extends SplashScreenContract.AbstractSplashPresenter {

    private final int TIME_OUT = 1;

    public SplashPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        super(presenterSubcomponentBuilders);
    }

    @Override
    public void attachView(SplashScreenContract.ISplashView view) {
        super.attachView(view);
        Observable.timer(TIME_OUT, TimeUnit.SECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    getViewState().navigateToFilmListScreen();
                    getViewState().closeScreen();
                });
    }
}
