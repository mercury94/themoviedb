package com.mercuriy94.themoviedb.presentation.module.splash.view;

import android.widget.ImageView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;
import com.mercuriy94.themoviedb.presentation.common.view.Layout;
import com.mercuriy94.themoviedb.presentation.module.app.App;
import com.mercuriy94.themoviedb.presentation.module.splash.SplashScreenContract;
import com.mercuriy94.themoviedb.presentation.module.splash.presenter.SplashPresenter;

import butterknife.BindView;

/**
 * Created by nikit on 11.11.2017.
 */
@Layout(R.layout.act_splash_layout)
public class SplashViewActivity extends BaseActivity implements SplashScreenContract.ISplashView {

    @InjectPresenter
    SplashScreenContract.AbstractSplashPresenter mPresenter;

    @BindView(R.id.act_splash_iv_logo)
    ImageView mIvLogo;

    @ProvidePresenter
    SplashScreenContract.AbstractSplashPresenter providePresenter() {
        return new SplashPresenter(App.getHasPresenterSubcomponentBuilders(this));
    }

    @Override
    public void navigateToFilmListScreen() {
        mNavigator.navigateToMoviesScreen(this);
    }

    @Override
    public void closeScreen() {
        finish();
    }
}
