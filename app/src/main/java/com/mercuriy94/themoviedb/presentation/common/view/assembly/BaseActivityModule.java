package com.mercuriy94.themoviedb.presentation.common.view.assembly;

import com.mercuriy94.themoviedb.presentation.common.di.activitybindings.ActivityModule;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;

import dagger.Module;

/**
 * Created by nikit on 11.11.2017.
 */

@Module
public class BaseActivityModule extends ActivityModule<BaseActivity> {

    public BaseActivityModule(BaseActivity baseActivity) {
        super(baseActivity);
    }


}
