package com.mercuriy94.themoviedb.presentation.common.di.repository;

import android.support.annotation.NonNull;

import com.mercuriy94.themoviedb.data.entity.Movie;
import com.mercuriy94.themoviedb.data.repositry.movie.local.IMovieLocalRepository;
import com.mercuriy94.themoviedb.data.repositry.movie.local.MovieLocalRepository;
import com.mercuriy94.themoviedb.presentation.common.di.database.DatabaseModule;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.objectbox.Box;
import io.objectbox.BoxStore;

/**
 * Created by nikit on 11.11.2017.
 */
@Module(includes = {DatabaseModule.class, LocalRepositoryModule.Declarations.class})
public class LocalRepositoryModule {

    @NonNull
    @Provides
    public Box<Movie> provideBoxCurrentUser(BoxStore boxStore) {
        return boxStore.boxFor(Movie.class);
    }

    @Module
    public static abstract class Declarations {

        @NonNull
        @Binds
        protected abstract IMovieLocalRepository bindMovieLocalRepository(MovieLocalRepository movieLocalRepositoryImpl);

    }

}
