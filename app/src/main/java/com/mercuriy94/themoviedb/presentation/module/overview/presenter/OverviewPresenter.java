package com.mercuriy94.themoviedb.presentation.module.overview.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.module.overview.OverviewScreenContract;

/**
 * Created by nikit on 14.11.2017.
 */

@InjectViewState
public class OverviewPresenter extends OverviewScreenContract.AbstractOverviewPresenter {

    @NonNull
    private final String mMovieTitle;
    @NonNull
    private final String mOverviewText;

    public OverviewPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders,
                             @NonNull String movieTitle,
                             @NonNull String overviewText) {
        super(presenterSubcomponentBuilders);
        mMovieTitle = movieTitle;
        mOverviewText = overviewText;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().showBackNavigationButton();
        getViewState().showTitle(mMovieTitle);
        getViewState().showOverviewText(mOverviewText);
    }

    @Override
    public void onClickBtnBack() {
        getViewState().close();
    }
}
