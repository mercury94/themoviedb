package com.mercuriy94.themoviedb.presentation.module.movies.strategy;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.ViewCommand;
import com.arellomobile.mvp.viewstate.strategy.StateStrategy;

import java.util.Iterator;
import java.util.List;

/**
 * Created by nikit on 13.11.2017.
 */

public class MoviesShowDialogStrategy implements StateStrategy {

    public static final String TAG_SHOW_DIALOG = "show_dialog";
    public static final String TAG_HIDE_DIALOG = "hide_dialog";

    @Override
    public <View extends MvpView> void beforeApply(List<ViewCommand<View>> currentState,
                                                   ViewCommand<View> incomingCommand) {

        if (incomingCommand.getTag().equals(TAG_SHOW_DIALOG) ||
                incomingCommand.getTag().contains(TAG_HIDE_DIALOG)) {
            removePendingViewCommand(incomingCommand, currentState);
            currentState.add(incomingCommand);
        }

    }

    @Override
    public <View extends MvpView> void afterApply(List<ViewCommand<View>> currentState, ViewCommand<View> incomingCommand) {
        //do nothing
    }

    private <View extends MvpView> void removePendingViewCommand(ViewCommand<View> incomingCommand,
                                                                 List<ViewCommand<View>> currentState) {
        for (Iterator<ViewCommand<View>> iterator = currentState.iterator(); iterator.hasNext(); ) {
            ViewCommand viewCommand = iterator.next();
            if (viewCommand.getTag() != null &&
                    (viewCommand.getTag().equals(TAG_SHOW_DIALOG)) ||
                    (viewCommand.getTag().equals(TAG_HIDE_DIALOG))) {
                iterator.remove();
                break;
            }
        }
    }
}