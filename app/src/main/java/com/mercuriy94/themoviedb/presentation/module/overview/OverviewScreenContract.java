package com.mercuriy94.themoviedb.presentation.module.overview;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView;

/**
 * Created by nikit on 14.11.2017.
 */

public class OverviewScreenContract {

    public OverviewScreenContract() {
        throw new RuntimeException("No instance please!");
    }

    public interface IOverviewView extends IBaseView {

        @StateStrategyType(AddToEndSingleStrategy.class)
        void showBackNavigationButton();

        @StateStrategyType(AddToEndSingleStrategy.class)
        void showTitle(String title);

        @StateStrategyType(AddToEndSingleStrategy.class)
        void showOverviewText(String overviewText);

        @StateStrategyType(SkipStrategy.class)
        void close();
    }

    public static abstract class AbstractOverviewPresenter extends BasePresenter<IOverviewView> {

        public AbstractOverviewPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }

        public abstract void onClickBtnBack();

    }


}
