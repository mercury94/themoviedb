package com.mercuriy94.themoviedb.presentation.common.di.repository;

import com.mercuriy94.themoviedb.data.repositry.movie.IMovieRepository;
import com.mercuriy94.themoviedb.data.repositry.movie.MovieRepository;

import dagger.Binds;
import dagger.Module;

/**
 * Created by nikit on 11.11.2017.
 */

@Module(includes = {WebRepositoryModule.class, LocalRepositoryModule.class})
public abstract class RepositoryModule {

    @Binds
    public abstract IMovieRepository bindMovieRepository(MovieRepository movieRepositoryImpl);

}
