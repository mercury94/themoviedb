package com.mercuriy94.themoviedb.presentation.module.app.assembly;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;

import com.mercuriy94.themoviedb.data.utils.INetworkHelper;
import com.mercuriy94.themoviedb.presentation.navigator.ActivityNavigator;
import com.mercuriy94.themoviedb.presentation.utils.NetworkHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @NonNull
    private final Context mContext;

    public AppModule(@NonNull Context context) {
        this.mContext = context;
    }

    @NonNull
    @Provides
    Context provideContext() {
        return mContext;
    }

    @Singleton
    @NonNull
    @Provides
    ActivityNavigator provideNavigator() {
        return new ActivityNavigator();
    }

    @Named("string_to_date")
    @Singleton
    @NonNull
    @Provides
    DateFormat provideStringToDateConversionFormat() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    }

    @Named("date_to_string_for_view")
    @Singleton
    @NonNull
    @Provides
    DateFormat provideDateToStringConversionFormatForView(Locale locale) {
        if (locale.getCountry().equals("RU")) {
            return new SimpleDateFormat("d MMMM, yyyy", locale);
        }

        return new SimpleDateFormat("MMMM d, yyyy", locale);
    }

    @Named("system_language")
    @Singleton
    @NonNull
    @Provides
    String provideSystemLanguage(Locale locale) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ?
                locale.toLanguageTag() :
                String.format("%s-%s", locale.getLanguage(), locale.getCountry());
    }

    @Singleton
    @NonNull
    @Provides
    Locale provideCurrentLocale() {
        Configuration configuration = Resources.getSystem().getConfiguration();
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                getSystemLocale(configuration) :
                getSystemLocaleLegacy(configuration);
    }

    @SuppressWarnings("deprecation")
    public Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }

    @Provides
    public INetworkHelper provideNetworkHelper() {
        return new NetworkHelper(mContext);
    }

}
