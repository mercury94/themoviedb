package com.mercuriy94.themoviedb.presentation.module.app;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.common.di.activitybindings.ActivityComponentBuilder;
import com.mercuriy94.themoviedb.presentation.common.di.activitybindings.HasActivitySubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.di.database.DatabaseModule;
import com.mercuriy94.themoviedb.presentation.common.di.network.NetworkModule;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.PresenterComponentBuilder;
import com.mercuriy94.themoviedb.presentation.common.di.repository.LocalRepositoryModule;
import com.mercuriy94.themoviedb.presentation.common.di.repository.WebRepositoryModule;
import com.mercuriy94.themoviedb.presentation.common.di.rxschedulers.RxSchedulerModule;
import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;
import com.mercuriy94.themoviedb.presentation.module.app.assembly.AppModule;
import com.mercuriy94.themoviedb.presentation.module.app.assembly.DaggerIAppComponent;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by nikit on 11.11.2017.
 */
@ReportsCrashes(mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.default_error_crash_message)
public class App extends MultiDexApplication implements HasPresenterSubcomponentBuilders,
        HasActivitySubcomponentBuilders {

    @Inject
    Map<Class<? extends BasePresenter>, Provider<PresenterComponentBuilder>> mPresenterComponentBuilders;

    @Inject
    Map<Class<? extends BaseActivity>, Provider<ActivityComponentBuilder>> mActivityComponentBuilders;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        ACRA.init(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        inject(this);
    }

    public static HasPresenterSubcomponentBuilders getHasPresenterSubcomponentBuilders(Context context) {
        return ((HasPresenterSubcomponentBuilders) context.getApplicationContext());
    }

    public static HasActivitySubcomponentBuilders getHasActivitySubcomponentBuilders(Context context) {
        return ((HasActivitySubcomponentBuilders) context.getApplicationContext());
    }

    public void inject(Context context) {
        DaggerIAppComponent.builder()
                .appModule(new AppModule(context))
                .networkModule(new NetworkModule())
                .databaseModule(new DatabaseModule())
                .localRepositoryModule(new LocalRepositoryModule())
                .webRepositoryModule(new WebRepositoryModule())
                .rxSchedulerModule(new RxSchedulerModule())
                .build()
                .inject(this);
    }

    //region HasPresenterSubcomponentBuilders

    @Override
    public PresenterComponentBuilder getPresenterComponentBuilder(Class<? extends BasePresenter> presenterClass) {
        return mPresenterComponentBuilders.get(presenterClass).get();
    }

    //endregion HasPresenterSubcomponentBuilders

    //region HasActivitySubcomponentBuilders

    @Override
    public ActivityComponentBuilder getActivityComponentBuilder(Class<? extends BaseActivity> presenterClass) {
        return mActivityComponentBuilders.get(presenterClass).get();
    }

    //endregion HasActivitySubcomponentBuilders
}
