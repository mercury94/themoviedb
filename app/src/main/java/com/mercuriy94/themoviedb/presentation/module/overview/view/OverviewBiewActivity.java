package com.mercuriy94.themoviedb.presentation.module.overview.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;
import com.mercuriy94.themoviedb.presentation.common.view.Layout;
import com.mercuriy94.themoviedb.presentation.module.app.App;
import com.mercuriy94.themoviedb.presentation.module.overview.OverviewScreenContract;
import com.mercuriy94.themoviedb.presentation.module.overview.presenter.OverviewPresenter;

import butterknife.BindView;

/**
 * Created by nikit on 14.11.2017.
 */

@Layout(R.layout.act_overview_layout)
public class OverviewBiewActivity extends BaseActivity
        implements OverviewScreenContract.IOverviewView {

    public static final String EXTRA_MOVIE_TITLE = "extra_movie_title";
    public static final String EXTRA_MOVIE_OVERVIEW = "extra_movie_overview";

    @InjectPresenter
    OverviewScreenContract.AbstractOverviewPresenter mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.act_overview_tv_text)
    TextView mTvOverview;

    public static Intent newIntent(Context context,
                                   String movieTitle,
                                   String overviewText) {
        Intent intent = new Intent(context, OverviewBiewActivity.class);
        intent.putExtra(EXTRA_MOVIE_TITLE, movieTitle);
        intent.putExtra(EXTRA_MOVIE_OVERVIEW, overviewText);
        return intent;
    }


    @ProvidePresenter
    OverviewScreenContract.AbstractOverviewPresenter providePresenter() {
        return new OverviewPresenter(App.getHasPresenterSubcomponentBuilders(this),
                getIntent().getStringExtra(EXTRA_MOVIE_TITLE),
                getIntent().getStringExtra(EXTRA_MOVIE_OVERVIEW));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(mToolbar);
    }

    @Override
    public void showTitle(String title) {
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(title);
    }

    @Override
    public void showBackNavigationButton() {
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showOverviewText(String overviewText) {
        mTvOverview.setText(overviewText);
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        mPresenter.onClickBtnBack();
        return true;
    }

    @Override
    public void onBackPressed() {
        mPresenter.onClickBtnBack();
    }
}
