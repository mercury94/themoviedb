package com.mercuriy94.themoviedb.presentation.module.image.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mercuriy94.themoviedb.R;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;
import com.mercuriy94.themoviedb.presentation.module.app.App;
import com.mercuriy94.themoviedb.presentation.module.image.ImageViewContract;
import com.mercuriy94.themoviedb.presentation.module.image.presenter.ImagePresenter;
import com.mercuriy94.themoviedb.presentation.utils.GlideApp;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nikit on 12.11.2017.
 */
public class ImageViewActivity extends BaseActivity implements ImageViewContract.IImageView {

    public static final String EXTRA_POSTER_PATH = "extra_poster_path";
    public static final String EXTRA_MOVIE_TITLE = "extra_movie_title";

    @InjectPresenter
    ImageViewContract.AbstractImagePresenter mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.act_image_zoom_iv_poster)
    ImageView mIvPoster;

    public static Intent newIntent(Context context, String posterPath, String movieTitle) {
        Intent intent = new Intent(context, ImageViewActivity.class);
        intent.putExtra(EXTRA_POSTER_PATH, posterPath);
        intent.putExtra(EXTRA_MOVIE_TITLE, movieTitle);
        return intent;
    }

    @ProvidePresenter
    public ImageViewContract.AbstractImagePresenter providePresenter() {
        return new ImagePresenter(App.getHasPresenterSubcomponentBuilders(this),
                getIntent().getStringExtra(EXTRA_POSTER_PATH),
                getIntent().getStringExtra(EXTRA_MOVIE_TITLE));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            setupWindowAnimations();
            postponeEnterTransition();
        }
        setContentView(R.layout.act_image_layout);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        Transition open = TransitionInflater.from(this).inflateTransition(R.transition.open_image_zoom_transition);
        Transition close = TransitionInflater.from(this).inflateTransition(R.transition.close_image_zoom_transition);
        getWindow().setExitTransition(close);
        getWindow().setEnterTransition(open);
    }

    @Override
    public void showTitle(String title) {
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for (int i = 0; i < mToolbar.getChildCount(); i++) {
                View viewToolbar = mToolbar.getChildAt(i);
                if (viewToolbar instanceof TextView) {
                    viewToolbar.setTransitionName("title_transition");
                    break;
                }
            }
        }
    }

    @Override
    public void showBackNavigationButton() {
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for (int i = 0; i < mToolbar.getChildCount(); i++) {
                View viewToolbar = mToolbar.getChildAt(i);
                if (viewToolbar instanceof ImageButton) {
                    viewToolbar.setTransitionName("back_btn_transition");
                    break;
                }
            }
        }
    }

    @Override
    public void showPoster(String imagePath) {
        GlideApp.with(this)
                .load(imagePath)
                .placeholder(R.drawable.image_paceholder)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        scheduleStartPostponedTransition(mIvPoster);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        scheduleStartPostponedTransition(mIvPoster);
                        return false;
                    }
                })
                .into(mIvPoster);
    }

    private void scheduleStartPostponedTransition(final View sharedElement) {
        sharedElement.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            startPostponedEnterTransition();
                        }
                        return true;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPresenter.onClickBtnBack();
    }

    @Override
    public boolean onSupportNavigateUp() {
        mPresenter.onClickBtnBack();
        return true;
    }


    @Override
    public void close() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) finishAfterTransition();
        else finish();
    }

}
