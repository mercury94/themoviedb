package com.mercuriy94.themoviedb.presentation.common.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.MvpPresenter;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView;

/**
 * Created by nikit on 11.11.2017.
 */

public abstract class BasePresenter<View extends IBaseView> extends MvpPresenter<View> {

    //region constructor

    public BasePresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        inject(presenterSubcomponentBuilders);
    }

    //endregion constructor

    //region MvpPresenter

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().prepareScreen();
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);

    }

    //endregion MvpPresenter

    //region this is class

    protected void inject(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        //do override
    }

    //endregion this is class

}
