package com.mercuriy94.themoviedb.presentation.common.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.ViewCommand;
import com.arellomobile.mvp.viewstate.strategy.StateStrategy;

import java.util.Iterator;
import java.util.List;

/**
 * Created by nikita on 02.07.17.
 */

public class AddToEndSingleByTagStrategy implements StateStrategy {

    public static final String TAG_SHOW = "show";
    public static final String TAG_HIDE = "hide";

    @Override
    public <View extends MvpView> void beforeApply(List<ViewCommand<View>> currentState,
                                                   ViewCommand<View> incomingCommand) {

        if (incomingCommand.getTag().equals(TAG_SHOW) ||
                incomingCommand.getTag().contains(TAG_HIDE)) {
            removePendingViewCommand(incomingCommand, currentState);
            currentState.add(incomingCommand);
        }

    }

    @Override
    public <View extends MvpView> void afterApply(List<ViewCommand<View>> currentState, ViewCommand<View> incomingCommand) {
        //do nothing
    }

    private <View extends MvpView> void removePendingViewCommand(ViewCommand<View> incomingCommand,
                                                                 List<ViewCommand<View>> currentState) {
        for (Iterator<ViewCommand<View>> iterator = currentState.iterator(); iterator.hasNext(); ) {
            ViewCommand viewCommand = iterator.next();
            if (viewCommand.getTag() != null &&
                    (viewCommand.getTag().equals(TAG_SHOW)) ||
                    (viewCommand.getTag().equals(TAG_HIDE))) {
                iterator.remove();
                break;
            }
        }
    }
}
