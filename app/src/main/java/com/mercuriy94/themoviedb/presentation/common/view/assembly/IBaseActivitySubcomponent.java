package com.mercuriy94.themoviedb.presentation.common.view.assembly;

import com.mercuriy94.themoviedb.presentation.common.di.activitybindings.ActivityComponent;
import com.mercuriy94.themoviedb.presentation.common.di.activitybindings.ActivityComponentBuilder;
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity;

import dagger.Subcomponent;

/**
 * Created by nikit on 11.11.2017.
 */
@Subcomponent(modules = BaseActivityModule.class)
public interface IBaseActivitySubcomponent extends ActivityComponent<BaseActivity> {

    @Subcomponent.Builder
    interface Builder extends ActivityComponentBuilder<BaseActivityModule, IBaseActivitySubcomponent> {

    }

}
