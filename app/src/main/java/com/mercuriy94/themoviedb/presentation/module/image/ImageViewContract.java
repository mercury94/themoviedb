package com.mercuriy94.themoviedb.presentation.module.image;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView;

/**
 * Created by nikit on 12.11.2017.
 */

public class ImageViewContract {

    private ImageViewContract() {
        throw new RuntimeException("no instance please!");
    }

    public interface IImageView extends IBaseView {

        @StateStrategyType(value = AddToEndSingleStrategy.class)
        void showTitle(String title);

        @StateStrategyType(value = AddToEndSingleStrategy.class)
        void showBackNavigationButton();

        @StateStrategyType(value = AddToEndSingleStrategy.class)
        void showPoster(String imagePath);

        @StateStrategyType(SkipStrategy.class)
        void close();
    }

    public static abstract class AbstractImagePresenter extends BasePresenter<IImageView> {

        public AbstractImagePresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }

        public abstract void onClickBtnBack();
    }

}
