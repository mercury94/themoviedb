package com.mercuriy94.themoviedb.presentation.module.movies;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.util.DiffUtil;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter;
import com.mercuriy94.themoviedb.presentation.common.view.AddToEndSingleByTagStrategy;
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;
import com.mercuriy94.themoviedb.presentation.module.movies.strategy.MoviesShowDataStrategy;
import com.mercuriy94.themoviedb.presentation.module.movies.strategy.MoviesShowDialogStrategy;
import com.mercuriy94.themoviedb.presentation.module.movies.strategy.MoviesShowEmptyListViewStrategy;

import java.util.List;

/**
 * Created by nikit on 11.11.2017.
 */

public class MoviesScreenContract {

    public static final int SORTING_BY_POPULAR = 0;
    public static final int SORTING_BY_TOP_RATED = 1;

    public MoviesScreenContract() {
        throw new RuntimeException("no instance please!");
    }

    public interface IMoviesView extends IBaseView {

        @StateStrategyType(AddToEndSingleStrategy.class)
        void showTitle(@StringRes int titleRes);

        @StateStrategyType(value = MoviesShowDataStrategy.class, tag = MoviesShowDataStrategy.TAG_SHOW_DATA)
        void showData(List<MovieModel> movies, DiffUtil.DiffResult diffResult);

        @StateStrategyType(SkipStrategy.class)
        void showSortPopupMenu(int currentSort);

        @StateStrategyType(value = MoviesShowDataStrategy.class, tag = MoviesShowDataStrategy.TAG_HIDE_DATA)
        void clearData();

        @StateStrategyType(value = MoviesShowEmptyListViewStrategy.class, tag = MoviesShowEmptyListViewStrategy.TAG_SHOW_EMPTY_LIST_VIEW)
        void showEmptyListView();

        @StateStrategyType(value = MoviesShowEmptyListViewStrategy.class, tag = MoviesShowEmptyListViewStrategy.TAG_HIDE_EMPTY_LIST_VIEW)
        void hideEmptyListView();

        @StateStrategyType(value = AddToEndSingleByTagStrategy.class, tag = AddToEndSingleByTagStrategy.TAG_SHOW)
        void showFooterPendingLoadMovies();

        @StateStrategyType(value = AddToEndSingleByTagStrategy.class, tag = AddToEndSingleByTagStrategy.TAG_SHOW)
        void showPendingLoadMovies(boolean loadFromRefreshing);

        @StateStrategyType(value = AddToEndSingleByTagStrategy.class, tag = AddToEndSingleByTagStrategy.TAG_SHOW)
        void showErrorLoadDataContent(boolean isFooter);

        @StateStrategyType(value = AddToEndSingleByTagStrategy.class, tag = AddToEndSingleByTagStrategy.TAG_HIDE)
        void hideErrorLoadDataContent();

        @StateStrategyType(value = MoviesShowDialogStrategy.class, tag = MoviesShowDialogStrategy.TAG_SHOW_DIALOG)
        void showDialogRequestRefreshData();

        @StateStrategyType(value = MoviesShowDialogStrategy.class, tag = MoviesShowDialogStrategy.TAG_HIDE_DIALOG)
        void hideDialogRequestRefreshData();

        @StateStrategyType(value = MoviesShowDialogStrategy.class, tag = MoviesShowDialogStrategy.TAG_SHOW_DIALOG)
        void showDialogOpenSettingsConnection();

        @StateStrategyType(value = MoviesShowDialogStrategy.class, tag = MoviesShowDialogStrategy.TAG_HIDE_DIALOG)
        void hideDialogOpenSettingsConnection();

        @StateStrategyType(SkipStrategy.class)
        void navigateToSettingsScreen();

        @StateStrategyType(SkipStrategy.class)
        void close();

    }

    public static abstract class AbstractMoviesPresenter extends BasePresenter<IMoviesView> {

        public AbstractMoviesPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }

        public abstract void onClickMenuSort();

        public abstract void onSelectedSortMenu(int sort);

        public abstract void onMoviesScrolled(int lastVisibleMoviesPosition);

        public abstract void onRefreshMovies();

        public abstract void onClickBtnRetryLoadData(boolean isFooter);

        public abstract void onClickBtnOpenSettings();

        public abstract void onClickBtnNoOpenSettings();

        public abstract void onClickBtnRefreshData();

        public abstract void onClickBtnNoRefreshData();

        public abstract void onChangeNetworkState(boolean isConnected);

        public abstract void onClickBtnBack();
    }

}
