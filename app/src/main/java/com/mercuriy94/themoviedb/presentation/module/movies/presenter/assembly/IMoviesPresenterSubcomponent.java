package com.mercuriy94.themoviedb.presentation.module.movies.presenter.assembly;

import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.PresenterComponent;
import com.mercuriy94.themoviedb.presentation.common.di.presenterbindings.PresenterComponentBuilder;
import com.mercuriy94.themoviedb.presentation.common.di.scope.PresenterScope;
import com.mercuriy94.themoviedb.presentation.module.movies.presenter.MoviesPresenter;

import dagger.Subcomponent;

/**
 * Created by nikit on 11.11.2017.
 */
@PresenterScope
@Subcomponent(modules = MoviesPresenterModule.class)
public interface IMoviesPresenterSubcomponent extends PresenterComponent<MoviesPresenter> {

    @Subcomponent.Builder
    interface Builder extends PresenterComponentBuilder<MoviesPresenterModule, IMoviesPresenterSubcomponent> {

    }

}
