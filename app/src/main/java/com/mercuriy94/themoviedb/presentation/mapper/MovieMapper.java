package com.mercuriy94.themoviedb.presentation.mapper;

import android.support.annotation.NonNull;

import com.annimon.stream.function.Function;
import com.mercuriy94.themoviedb.data.entity.Movie;
import com.mercuriy94.themoviedb.data.mapper.Mapper;
import com.mercuriy94.themoviedb.presentation.model.MovieModel;

import java.text.DateFormat;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by nikit on 11.11.2017.
 */

public class MovieMapper extends Mapper<Movie, MovieModel> {

    @Named("date_to_string_for_view")
    @Inject
    DateFormat mFormat;


    @Inject
    public MovieMapper() {
    }

    @NonNull
    @Override
    protected Function<Movie, MovieModel> transform() {
        return movie -> {
            MovieModel movieModel = new MovieModel();
            movieModel.setId(movie.getId());
            movieModel.setPreviewImageFullPath(movie.getPreviewImageFullPath());
            movieModel.setPosterPath(movie.getPosterPath());
            movieModel.setPosterFullPath(movie.getPosterFullPath());
            movieModel.setAdult(movie.isAdult());
            movieModel.setOverview(movie.getOverview());
            if (movie.getReleaseDate() != null) {
                movieModel.setReleaseDate(mFormat.format(movie.getReleaseDate()));
            }
            movieModel.setOriginalTitle(movie.getOriginalTitle());
            movieModel.setOriginalLanguage(movie.getOriginalLanguage());
            movieModel.setTitle(movie.getTitle());
            movieModel.setBackDropPath(movie.getBackDropPath());
            movieModel.setPopularity(movie.getPopularity());
            movieModel.setVoteCount(movie.getVoteCount());
            movieModel.setVideo(movie.isVideo());
            movieModel.setVoteAverage(round(movie.getVoteAverage(), 2));
            return movieModel;
        };
    }


    private float round(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) pow *= 10;
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
    }
}
