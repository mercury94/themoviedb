package com.mercuriy94.themoviedb.domain.movie;

import android.support.annotation.NonNull;

import com.mercuriy94.themoviedb.data.entity.Movie;
import com.mercuriy94.themoviedb.data.repositry.movie.IMovieRepository;
import com.mercuriy94.themoviedb.domain.common.BaseInteractor;
import com.mercuriy94.themoviedb.presentation.common.di.rxschedulers.RxSchedulerModule;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Created by nikit on 11.11.2017.
 */

public class FetchMoviesInteractor extends BaseInteractor<List<Movie>, FetchMoviesInteractor.Params> {

    public static final int SORT_BY_POPULAR = 0;
    public static final int SORT_BY_TOP_RATED = 1;

    private final int FIRST_PAGE = 1;

    @Inject
    IMovieRepository mMovieRepository;
    private int mCurrentPage = 1;
    private int mCurrentSorting;
    private boolean mAllLoadData = false;

    @Inject
    public FetchMoviesInteractor(
            @Named(RxSchedulerModule.COMPUTATION) @NonNull Scheduler subscriberScheduler,
            @Named(RxSchedulerModule.MAIN) @NonNull Scheduler observerScheduler) {
        super(subscriberScheduler, observerScheduler);
    }

    @Override
    protected Observable<List<Movie>> buildObservable(Params params) {
        if (mCurrentSorting != params.mSorting) releasePage();
        mCurrentSorting = params.mSorting;
        return fetchMovies().doOnNext(movies -> {
            if (!movies.isEmpty()) mCurrentPage++;
            else mAllLoadData = true;
        });
    }

    private Observable<List<Movie>> fetchMovies() {
        return mCurrentSorting == SORT_BY_POPULAR ?
                mMovieRepository.fetchPopularMovies(mCurrentPage) :
                mMovieRepository.fetchTopRatedMovies(mCurrentPage);
    }

    public void releasePage() {
        mAllLoadData = false;
        mCurrentPage = FIRST_PAGE;
    }


    public boolean isAllLoadData() {
        return mAllLoadData;
    }


    public static class Params {

        private final int mSorting;

        public Params(int sorting) {
            mSorting = sorting;
        }
    }

}
