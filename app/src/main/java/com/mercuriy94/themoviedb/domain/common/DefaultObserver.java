package com.mercuriy94.themoviedb.domain.common;

import org.json.JSONObject;

import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

/**
 * Created by nikita on 02.06.17.
 */

public abstract class DefaultObserver<T> extends DisposableObserver<T> {

    @Override
    protected void onStart() {
        //do override
    }

    @Override
    public void onNext(T t) {
        //do override
    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) e).response().errorBody();
            onHttpError(e, getErrorMessage(responseBody));
        } else {
            onUnknownError(e, e.getMessage());
        }
    }

    private String getErrorMessage(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString("status_message");
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    protected void onHttpError(Throwable throwable, String message) {
        //do override
    }

    protected void onUnknownError(Throwable throwable, String message) {
        //do override
    }

    @Override
    public void onComplete() {
        //do override
    }
}