package com.mercuriy94.themoviedb.data.repositry.movie;

import android.support.annotation.NonNull;

import com.mercuriy94.themoviedb.data.entity.Movie;
import com.mercuriy94.themoviedb.data.entity.dto.MovieDto;
import com.mercuriy94.themoviedb.data.entity.rest.response.FetchMoviesResponse;
import com.mercuriy94.themoviedb.data.mapper.MovieDtoMapper;
import com.mercuriy94.themoviedb.data.repositry.movie.local.IMovieLocalRepository;
import com.mercuriy94.themoviedb.data.repositry.movie.web.IMovieWebRepository;
import com.mercuriy94.themoviedb.data.utils.INetworkHelper;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by nikit on 11.11.2017.
 */

public class MovieRepository implements IMovieRepository {

    @Inject
    INetworkHelper mNetworkHelper;

    @Inject
    IMovieWebRepository mWebRepository;

    @Inject
    IMovieLocalRepository mLocalRepository;

    @Inject
    MovieDtoMapper mMovieDtoMapper;

    @Inject
    public MovieRepository() {

    }

    @Override
    public Observable<List<Movie>> fetchPopularMovies(int page) {
        return Observable.fromCallable(() -> mNetworkHelper.isNetworkConnected())
                .flatMap(isNetworkConnected -> isNetworkConnected ?
                        getRemotePopularMovies(page) :
                        mLocalRepository.fetchPopularMovies(page));
    }

    @Override
    public Observable<List<Movie>> fetchTopRatedMovies(int page) {
        return Observable.fromCallable(() -> mNetworkHelper.isNetworkConnected())
                .flatMap(isNetworkConnected -> isNetworkConnected ?
                        getRemoteTopRatedMovies(page) :
                        mLocalRepository.fetchTopRatedMovies(page));
    }

    private Observable<List<Movie>> getRemotePopularMovies(int page) {
        return mWebRepository.fetchPopularMovies(page)
                .map(FetchMoviesResponse::getResult)
                .map(transform())
                .flatMap(this::cache);
    }

    private Observable<List<Movie>> getRemoteTopRatedMovies(int page) {
        return mWebRepository.fetchTopRatedMovies(page)
                .map(FetchMoviesResponse::getResult)
                .map(transform())
                .flatMap(this::cache);
    }

    @NonNull
    private Function<List<MovieDto>, List<Movie>> transform() {
        return movieDtos -> mMovieDtoMapper.execute(movieDtos);
    }

    private Observable<List<Movie>> cache(List<Movie> movies) {
        return mLocalRepository.put(movies);
    }


}
