package com.mercuriy94.themoviedb.data.repositry.movie.local;

import com.mercuriy94.themoviedb.data.entity.Movie;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nikit on 11.11.2017.
 */

public interface IMovieLocalRepository {

    Observable<List<Movie>> fetchPopularMovies(int page);

    Observable<List<Movie>> fetchTopRatedMovies(int page);

    Observable<List<Movie>> put(List<Movie> movies);

    Observable<Boolean> removeAll();

}
