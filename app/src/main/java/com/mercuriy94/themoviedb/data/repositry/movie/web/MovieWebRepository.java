package com.mercuriy94.themoviedb.data.repositry.movie.web;

import com.mercuriy94.themoviedb.data.constant.WebServiceConstant;
import com.mercuriy94.themoviedb.data.entity.rest.response.FetchMoviesResponse;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;

/**
 * Created by nikit on 11.11.2017.
 */

public class MovieWebRepository implements IMovieWebRepository {

    @Inject
    IMovieWebService mWebService;

    @Named("system_language")
    @Inject
    String mLanguage;

    @Inject
    public MovieWebRepository() {
    }

    @Override
    public Observable<FetchMoviesResponse> fetchPopularMovies(int page) {
        return mWebService.fetchPopularMovies(WebServiceConstant.API_KEY, mLanguage, page);
    }

    @Override
    public Observable<FetchMoviesResponse> fetchTopRatedMovies(int page) {
        return mWebService.fetchTopRatedMovies(WebServiceConstant.API_KEY, mLanguage, page);
    }
}
