package com.mercuriy94.themoviedb.data.constant;

/**
 * Created by nikit on 11.11.2017.
 */

public class WebServiceConstant {

    //region common

    public static final String API_KEY = "4710d4a540dfda7c6ccc8ece01a89e7e";

    //region common

    //region urls

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/";

    public static final String MOVIE_URL = BASE_URL + "movie";
    public static final String POPULAR_MOVIES_URL = MOVIE_URL + "/popular";
    public static final String TOP_RATED_MOVIES_URL = MOVIE_URL + "/top_rated";

    public static final String IMAGE_PREVIEW_URL = BASE_IMAGE_URL + "w342";
    public static final String IMAGE_POSTER_URL = BASE_IMAGE_URL + "w780";

    //endregion urls

}
