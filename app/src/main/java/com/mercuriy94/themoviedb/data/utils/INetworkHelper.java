package com.mercuriy94.themoviedb.data.utils;

/**
 * Created by nikita on 13.11.17.
 */

public interface INetworkHelper {

    boolean isNetworkConnected();

}
