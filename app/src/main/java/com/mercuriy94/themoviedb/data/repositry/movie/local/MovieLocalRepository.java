package com.mercuriy94.themoviedb.data.repositry.movie.local;


import com.mercuriy94.themoviedb.data.entity.Movie;
import com.mercuriy94.themoviedb.data.entity.Movie_;

import java.util.List;

import javax.inject.Inject;

import io.objectbox.Box;
import io.reactivex.Observable;

/**
 * Created by nikit on 11.11.2017.
 */

public class MovieLocalRepository implements IMovieLocalRepository {

    public static final int PAGE_LIMIT_MOVIES = 20;

    @Inject
    Box<Movie> mMovieBox;

    @Inject
    public MovieLocalRepository() {
    }

    @Override
    public Observable<List<Movie>> fetchPopularMovies(int page) {
        return Observable.fromCallable(() -> mMovieBox.query()
                .orderDesc(Movie_.popularity)
                .build()
                .find((page - 1) * PAGE_LIMIT_MOVIES, PAGE_LIMIT_MOVIES));
    }

    @Override
    public Observable<List<Movie>> fetchTopRatedMovies(int page) {
        return Observable.fromCallable(() -> mMovieBox.query()
                .orderDesc(Movie_.voteAverage)
                .build()
                .find((page - 1) * PAGE_LIMIT_MOVIES, PAGE_LIMIT_MOVIES));
    }

    @Override
    public Observable<List<Movie>> put(List<Movie> movies) {
        return Observable.fromCallable(() -> {
            mMovieBox.put(movies);
            return movies;
        });
    }

    @Override
    public Observable<Boolean> removeAll() {
        return Observable.fromCallable(() -> {
            mMovieBox.removeAll();
            return true;
        });
    }

}
