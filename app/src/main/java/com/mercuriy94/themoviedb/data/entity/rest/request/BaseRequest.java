package com.mercuriy94.themoviedb.data.entity.rest.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nikit on 11.11.2017.
 */

public class BaseRequest {

    @SerializedName("api_key")
    private String mApiKey;
    @SerializedName("language")
    private String mLanguage;

    public String getApiKey() {
        return mApiKey;
    }

    public void setApiKey(String apiKey) {
        mApiKey = apiKey;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }
}
