package com.mercuriy94.themoviedb.data.entity.rest.response;

import com.google.gson.annotations.SerializedName;
import com.mercuriy94.themoviedb.data.entity.dto.MovieDto;

import java.util.List;

/**
 * Created by nikit on 11.11.2017.
 */

public class FetchMoviesResponse {

    @SerializedName("page")
    private int mPage;
    @SerializedName("total_pages")
    private int mTotalPages;
    @SerializedName("results")
    private List<MovieDto> mResult;
    @SerializedName("total_results")
    private int mTotalResults;

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        mPage = page;
    }

    public int getTotalResults() {
        return mTotalResults;
    }

    public void setTotalResults(int totalResults) {
        mTotalResults = totalResults;
    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(int totalPages) {
        mTotalPages = totalPages;
    }

    public List<MovieDto> getResult() {
        return mResult;
    }

    public void setResult(List<MovieDto> result) {
        mResult = result;
    }
}
