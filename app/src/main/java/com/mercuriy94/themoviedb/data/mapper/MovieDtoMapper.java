package com.mercuriy94.themoviedb.data.mapper;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.annimon.stream.function.Function;
import com.mercuriy94.themoviedb.data.constant.WebServiceConstant;
import com.mercuriy94.themoviedb.data.entity.Movie;
import com.mercuriy94.themoviedb.data.entity.dto.MovieDto;

import java.text.DateFormat;
import java.text.ParseException;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by nikit on 11.11.2017.
 */

public class MovieDtoMapper extends Mapper<MovieDto, Movie> {

    @Named("string_to_date")
    @Inject
    DateFormat format;

    @Inject
    public MovieDtoMapper() {

    }

    @NonNull
    @Override
    protected Function<MovieDto, Movie> transform() {
        return movieDto -> {
            Movie movie = new Movie();
            movie.setId(movieDto.getId());
            if (!TextUtils.isEmpty(movieDto.getPosterPath())) {
                movie.setPreviewImageFullPath(WebServiceConstant.IMAGE_PREVIEW_URL + movieDto.getPosterPath());
                movie.setPosterPath(movieDto.getPosterPath());
                movie.setPosterFullPath(WebServiceConstant.IMAGE_POSTER_URL + movieDto.getPosterPath());
            }
            movie.setAdult(movieDto.isAdult());
            movie.setOverview(movieDto.getOverview());
            try {
                movie.setReleaseDate(format.parse(movieDto.getReleaseDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            movie.setOriginalTitle(movieDto.getOriginalTitle());
            movie.setOriginalLanguage(movieDto.getOriginalLanguage());
            movie.setTitle(movieDto.getTitle());
            movie.setBackDropPath(movieDto.getBackDropPath());
            movie.setPopularity(movieDto.getPopularity());
            movie.setVoteCount(movieDto.getVoteCount());
            movie.setVideo(movieDto.isVideo());
            movie.setVoteAverage(movieDto.getVoteAverage());
            return movie;
        };
    }
}
