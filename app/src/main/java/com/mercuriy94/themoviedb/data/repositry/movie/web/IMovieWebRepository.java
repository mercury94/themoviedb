package com.mercuriy94.themoviedb.data.repositry.movie.web;

import com.mercuriy94.themoviedb.data.entity.rest.response.FetchMoviesResponse;

import io.reactivex.Observable;

/**
 * Created by nikit on 11.11.2017.
 */
public interface IMovieWebRepository {

    Observable<FetchMoviesResponse> fetchPopularMovies(int page);

    Observable<FetchMoviesResponse> fetchTopRatedMovies(int page);
}
