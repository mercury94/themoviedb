package com.mercuriy94.themoviedb.data.mapper;

import android.support.annotation.NonNull;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;

import java.util.List;

/**
 * Created by nikita on 03.06.17.
 */

public abstract class Mapper<Source, Destination> {

    public Mapper() {

    }

    @NonNull
    protected abstract Function<Source, Destination> transform();

    @NonNull
    public List<Destination> execute(@NonNull List<Source> sourceList) {
        return Stream.of(sourceList).map(this::execute).toList();
    }

    @NonNull
    public Destination execute(@NonNull Source source) {
        return transform().apply(source);
    }
}