package com.mercuriy94.themoviedb.data.entity.rest.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nikit on 11.11.2017.
 */

public class FetchMoviesRequest extends BaseRequest {

    @SerializedName("page")
    private int mPage;
    @SerializedName("region")
    private String mRegion;

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        mPage = page;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }
}
