package com.mercuriy94.themoviedb.data.repositry.movie.web;

import com.mercuriy94.themoviedb.data.constant.WebServiceConstant;
import com.mercuriy94.themoviedb.data.entity.rest.response.FetchMoviesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nikit on 11.11.2017.
 */
public interface IMovieWebService {

    @GET(WebServiceConstant.POPULAR_MOVIES_URL)
    Observable<FetchMoviesResponse> fetchPopularMovies(@Query("api_key") String apiKey,
                                                       @Query("language") String language,
                                                       @Query("page") int page);

    @GET(WebServiceConstant.TOP_RATED_MOVIES_URL)
    Observable<FetchMoviesResponse> fetchTopRatedMovies(@Query("api_key") String apiKey,
                                                        @Query("language") String language,
                                                        @Query("page") int page);

}
